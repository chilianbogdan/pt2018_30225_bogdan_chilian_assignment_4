package bk;

public class SpendingAccount extends Account {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double comision=0.5;

	
	public SpendingAccount(int accountId, Person pers, float amount) {
		super(accountId, pers, amount);
	}

	public void deposit(double val) {
			super.deposit(val);
	}

	public void extract(double val) {
		super.extract(val + val*comision);
	}

	public double getAmount() {
		amount -= comision;
		return amount;
	}
}

