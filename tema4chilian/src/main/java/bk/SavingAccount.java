package bk;

public class SavingAccount extends Account {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double dobanda = 2.75;

	public SavingAccount(int accountId, Person pers, float amount) {
		super(accountId, pers, amount);
	}

	public void deposit(double val) {
		super.deposit(val + dobanda * val);
	}
}
