package bk;

import java.io.Serializable;

public class Person implements Serializable {
	
	private static final long serialVersionUID = 9044329890463579286L;
	private String cnp;
	private String personName;
	private String personAddress;
	private String phone;
	
	
	public Person(String cnp, String name, String address, String phone){
		this.cnp=cnp;
		this.personName=name;
		this.personAddress=address;
		this.phone=phone;
		System.out.println ("A new person has been added with the following data:\n"+"Person ID: "+cnp+"\n"+
		"Name: "+personName+"\n"+"Address: "+personAddress+"\n"+"Phone: "+phone);
	}
	
	
	public String getCnp() {
		return cnp;
	}
	
	public void setCnp(String cnp) {
		this.cnp = cnp;
	}
	
	public String getPersonName() {
		return personName;
	}
	
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	
	public String getPersonAddress() {
		return personAddress;
	}
	
	public void setPersonAddress(String personAddress) {
		this.personAddress = personAddress;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String toString() {
		String cl  = cnp + " " + personName + " " + personAddress + " " + phone;
		return cl;
	}
}