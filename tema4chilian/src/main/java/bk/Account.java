package bk;

import java.io.Serializable;

public class Account implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int accountId;
	private Person accountOwner;
	protected double amount;

	public Account(int id, Person owner, double amount) {
		this.accountId = id;
		this.accountOwner = owner;
		this.amount = amount;
		System.out.println("A new account has been created with the following data: " + "\n" + "ID: " + this.accountId
				+ "\n" + "Owner: " + this.accountOwner + "\n" + "Ammount: " + this.amount);
	}
	public Account(Person owner, double amount) {
		this.accountOwner = owner;
		this.amount = amount;
	}
	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public Person getAccountOwner() {
		return accountOwner;
	}

	public void setAccountOwner(Person accountOwner) {
		this.accountOwner = accountOwner;
	}

	public void extract(double amount) {
		if (amount > this.amount)
			System.out.println("You do not have enough money!");
		else {
			this.amount -= amount;
			System.out.println("Extracted " + amount);
		}
	}

	public void deposit(double a) {
		this.amount += a;
		System.out.println("You deposited: " + a + ". You now have: " + this.amount);
	}

	public Person getOwner() {
		return accountOwner;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String toString() {
		String acc = accountId + " " + amount + " " + accountOwner.toString();
		return acc;
	}

}
