package bk;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class Bank implements BankProc, Serializable {
	private Hashtable<String, ArrayList<Account>> hash;

	/**
	 * Constructorul creaza o banca. Datele bancii sunt extrase dintr-un fisier
	 * de pe disc, sau se asteapta ca sa fie completate ulterior
	 */
	public Bank() {
		hash = new Hashtable<String, ArrayList<Account>>(100);
		if (this.isRestoring() == true)
			hash = this.restoreBanca();
	}

	/**
	 * Metoda adauga un cont in tabela de dispersie. Corectitudinea operatiei se
	 * verifica daca numarul de conturi creste cu 1.
	 * 
	 * @param: contul pe care il adaugam in banca
	 */
	public void addAccount(Account a) {
		assert a != null : "Eroare addAcount: account null!";
		assert isWellFormed() : "Tabelul nu e bine format!(eroare adaugare)";
		int dim = this.size();
		ArrayList<Account> conturi = hash.get(a.getAccountOwner().getCnp());
	
		if (conturi == null)
			conturi = new ArrayList<Account>();
		conturi.add(a);
		hash.put(a.getAccountOwner().getCnp(), conturi);
		assert isWellFormed() : "Tabelul nu e bine format!(eroare adaugare)";
		assert (dim + 1 == this.size()) : "Eroare la adaugare cont!";
	}

	/**
	 * Metoda sterge un cont specificat ca parametru, in cazul in care contul
	 * exista in tabel. Corectitudinea metodei se verifica prin faptul ca
	 * numarul de conturi va scadea cu 1.
	 * 
	 * @param: contul
	 *             pe care il sterg din banca;
	 */
	public void removeAccount(Account a) {

		assert a != null : "Eroare stergere: account null";
		assert isWellFormed() : "Tabelul nu e bine format!(eroare stergere cont)";
		int dim = this.size();
		ArrayList<Account> acc = hash.get(a.getAccountOwner().getCnp());
		if (acc.size() == 1)
			removePerson(a.getAccountOwner().getCnp());
		else {
			Iterator<Account> iterator = acc.iterator();
			while (iterator.hasNext()) {
				Account aCurent = (Account) iterator.next();
				if (a.getAccountId()==aCurent.getAccountId())
					iterator.remove();
			}
			hash.put(a.getAccountOwner().getCnp(), acc);
		}
		assert isWellFormed() : "Tabelul nu e bine format!(eroare stergere cont)";
		assert this.size() == dim - 1 : "Eroare la stergerea contului " + a.getAccountId();
	}

	/**
	 * Metoda sterge o cheie din tabelul de dispersie. Corectitudinea metodei e
	 * verificata prin faptul ca in tabel exista cheia si numarul de intrari in
	 * tabel scade cu 1.
	 * 
	 * @param cnp:
	 *            cheia pe care o eliminam din tabelul de dispersie;
	 */
	public void removePerson(String cnp) {
		assert hash.containsKey(cnp) : "Hashtable-ul nu contine cheia " + cnp;
		assert isWellFormed() : "Tabelul nu e bine format!(eroare stergere persoana)";
		int dim = hash.size();
		hash.remove(cnp);
		assert isWellFormed() : "Tabelul nu e bine format!(eroare stergere persoana)";
		assert hash.size() == dim - 1 : "Eroare la stergerea persoanei " + cnp;
	}

	/**
	 * Metoda returneaza o lista de conturi pentru o anumita persoana,
	 * specificata prin cnp. Cheie de identificare trebuie sa fie valida
	 * 
	 * @param: cheia
	 *             pentru care se returneaza lista de conturi
	 * @return: lista de conturi pe care le are persoana specificata
	 */
	public ArrayList<Account> readAccount(String key) {
		assert key != null : "Cheie nula!";
		assert isWellFormed() : "Tabelul nu e bine format!(eroare readAccount)";
		return hash.get(key);
	}
	
	/**
	 * Metoda scrie pe disc banca, pentru a putea pastra datele modificate in
	 * banca.
	 */
	public void writeAccount(Hashtable<String, ArrayList<Account>> h) {
		try {
			assert !hash.isEmpty() : "Eroare la serializare: hashtable vid";
			assert isWellFormed() : "Tabelul nu e bine format!(eroare scriere cont)";
			FileOutputStream file = new FileOutputStream("banca.txt");
			ObjectOutputStream obj = new ObjectOutputStream(file);
			obj.writeObject(h);
			obj.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Eroare la serializare! " + e);
		}
	}

	/**
	 * Metoda returneaza o lista a tuturor conturilor existente in banca, in
	 * cazul ca tabelul de dispersie nu e vid.
	 * 
	 * @return: lista tuturor conturilor din banca;
	 */
	public ArrayList<Account> report() {
		assert !hash.isEmpty() : "Eroare la report: hashtable gol!";
		assert isWellFormed() : "Tabelul nu e bine format!(eroare report)";
		Enumeration<String> chei = hash.keys();
		ArrayList<Account> raport = new ArrayList<Account>();
		while (chei.hasMoreElements()) {
			String cheie = (String) chei.nextElement();
			raport.addAll(hash.get(cheie));
		}
		return raport;
	}

	/**
	 * Metoda returneaza numarul de conturi pe care le contine tabela de
	 * dispersie.
	 * 
	 * @return numarul de conturi din tabela de dispersie.
	 */
	public int size() {
		int dim = 0;
		Enumeration<String> chei = hash.keys();
		ArrayList<Account> acc = new ArrayList<Account>();
		while (chei.hasMoreElements()) {
			String cheie = (String) chei.nextElement();
			acc = hash.get(cheie);
			Iterator<Account> it = acc.iterator();
			while (it.hasNext()) {
				it.next();
				dim++;
			}
		}
		return dim;
	}

	/**
	 * Metoda verifaca daca obiectul banca este bine format.
	 * 
	 * @return o valoare booleana care spune daca obiectul este bine format sau
	 *         nu;
	 */
	public boolean isWellFormed() {
		int nrKey = 0, nrAc;
		ArrayList<Account> a;
		Enumeration<String> chei = hash.keys();
		while (chei.hasMoreElements()) {
			String cheie = (String) chei.nextElement();
			a = hash.get(cheie);
			Iterator<Account> it = a.iterator();
			nrAc = 0;
			while (it.hasNext()) {
				Account acount = it.next();
				if (acount.getAccountOwner().getCnp().equals(cheie) == false)
					return false;
				nrAc++;
			}
			if (nrAc != a.size())
				return false;
			nrKey++;
		}
		if (nrKey != hash.size())
			
			return false;
		return true;
		
	}

	/**
	 * Metoda returneaza o tabela de dispersie care contine datele extrase
	 * dintr-un fisier de pe disc.
	 * 
	 * @return tabela de dispersie refacuta
	 */
	public Hashtable<String, ArrayList<Account>> restoreBanca() {
		try {
			FileInputStream file = new FileInputStream("banca.txt");
			ObjectInputStream obj = new ObjectInputStream(file);
			@SuppressWarnings("unchecked")
			Hashtable<String, ArrayList<Account>> h = (Hashtable<String, ArrayList<Account>>) obj.readObject();
			obj.close();
			return h;
		} catch (Exception e) {
			System.out.print("err deserializare ");
			return null;
		}
		
	}

	/**
	 * Metoda verifica daca banca se poate deserializa de pe disc
	 * 
	 * @return o valoare booleana care spune daca se poate deserializa banca
	 */
	public boolean isRestoring() {
		if (restoreBanca() != null)
			return true;
		return false;
	}

	/**
	 * Metoda returneaza tabela de dispersie care memoreaza datele bancii
	 * 
	 * @return tabela de dispersie a bancii
	 */
	public Hashtable<String, ArrayList<Account>> getHash() {
		return hash;
	}

	public void readAccounts() {
		// TODO Auto-generated method stub
		
	}

	public void writeAccounts() {
		// TODO Auto-generated method stub
		
	}

	public void deleteAccount(Account acc) {
		// TODO Auto-generated method stub
		
	}

	public Account getAccount(int acc) {
		// TODO Auto-generated method stub
		return null;
	}

	public double depositMoneyIntoAccount(int accountId, double money) {
		// TODO Auto-generated method stub
		return 0;
	}

	public double extractMoneyIntoAccount(int accountId, double money) {
		// TODO Auto-generated method stub
		return 0;
	}
}
