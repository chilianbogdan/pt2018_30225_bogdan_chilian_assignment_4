package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.PrintStream;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


import bk.Bank;
import bk.Person;
import bk.SavingAccount;
import bk.SpendingAccount;




public class GUI extends JFrame {

	/**
	 * This class creates the GUI
	 * The idea is to have a back panel which contains other panels on which certain components are added
	 * @author Vlad Itu
	 */
	private static final long serialVersionUID = -6911007649473080398L;
	private JPanel backPanel = new JPanel();
	private JPanel operationsPanel = new JPanel();

	private JDialog findDialog = new JDialog();
	private JDialog deleteDialog = new JDialog();
	private JDialog newPersonDialog = new JDialog();
	private JDialog newAccountDialog = new JDialog();
	private JDialog accountBalanceDialog = new JDialog();
	private JDialog extractDialog = new JDialog();
	private JDialog depositDialog = new JDialog();

	private JLabel entryNumberLabel = new JLabel ("Enter entry number");
	private JLabel deleteKeyLabel = new JLabel ("Enter key");
	private JLabel personIdLabel = new JLabel ("Id");
	private JLabel personNameLabel = new JLabel ("Name");
	private JLabel personAddressLabel = new JLabel ("Address");
	private JLabel accountIdLabel = new JLabel ("Id");
	private JLabel accountOwnerLabel = new JLabel ("Owner");
	private JLabel accountAmountLabel = new JLabel ("Amount");
	private JLabel ownerNameLabel = new JLabel ("Owner");
	private JLabel interestLabel = new JLabel ("Interest");
	private JLabel minimumMonthsLabel = new JLabel ("Minimum months");

	private JTextField entryNumber = new JTextField();
	private JTextField deleteKeyField = new JTextField();
	private JTextField personNameField = new JTextField();
	private JTextField personAddressField = new JTextField();
	private JTextField personIdField = new JTextField();
	private JTextField accountIdField = new JTextField();
	private JTextField accountAmountField = new JTextField();
	private JTextField extractAmountField = new JTextField();
	private JTextField depositAmountField = new JTextField();
	private JTextField interestField = new JTextField();
	private JTextField minimumMonthsField = new JTextField();

	private JButton findOk = new JButton("Find");
	private JButton insertOk = new JButton ("Insert");
	private JButton deleteOk = new JButton ("Delete");
	private JButton newPersonButton = new JButton ("New person");
	private JButton newPersonOkButton = new JButton ("Add person");
	private JButton newAccountButton = new JButton ("New account");
	private JButton newAccountOkButton = new JButton ("Add account");
	private JButton accountBalanceButton = new JButton ("Show balance");
	private JButton accountBalanceOkButton = new JButton ("Balance");
	private JButton extractButton = new JButton ("Extract");
	private JButton depositButton = new JButton ("Deposit");

	private JComboBox<?> opCombo;
	private JComboBox<String> personCombo;
	private JComboBox<String> ownerCombo;
	private JComboBox<String> ownerExtractCombo;
	private JComboBox<String> ownerDepositCombo;

	private JRadioButton personHash = new JRadioButton("Person");
	private JRadioButton accountHash = new JRadioButton("Account");
	private JRadioButton savingAccount = new JRadioButton("Saving account");
	private JRadioButton spendingAccount = new JRadioButton("Spending account");

	private Bank bank;
	private boolean okAccount=true;
	private boolean isSavingAccount=true;
	private boolean saved = true;
	private JTextArea ta ;
	//private TextAreaOutputStream taos ;
	private PrintStream ps ;
	private JScrollPane sbrText;

	public GUI(){
		ta= new JTextArea();
		try {
		//	taos= new TextAreaOutputStream( ta, 60 );
		} catch (Exception e1) {

			e1.printStackTrace();
		}
		//ps= new PrintStream( taos );
		System.setOut( ps );
		System.setErr( ps );
		bank = new Bank();
		setSize(500,500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setContentPane(backPanel);
		personCombo = new JComboBox<String>();
		ownerCombo = new JComboBox<String>();
		ownerExtractCombo = new JComboBox<String>();
		ownerDepositCombo = new JComboBox<String>();

		String[] opStrings = {"Select an operation", "Delete","Find","Show","Extract","Deposit","Save"};

		opCombo = new JComboBox<Object>(opStrings);
		opCombo.setSelectedIndex(0);

		ButtonGroup group = new ButtonGroup();
		group.add(personHash);
		group.add(accountHash);


		operationsPanel.add(opCombo);
		operationsPanel.add(personHash);
		operationsPanel.add(accountHash);
		operationsPanel.add(newPersonButton);
		operationsPanel.add(newAccountButton);
		operationsPanel.add(accountBalanceButton);

		backPanel.setLayout(new GridLayout(2,1));
		backPanel.add(operationsPanel);
		sbrText = new JScrollPane(ta);
		backPanel.add(sbrText);
		addListeners();
		addWindowListener();
		//bank.checkInterest();
	}
	/**
	 * Method to add a windows listener
	 * Useful to prompt the user to save if any modifications
	 */
	private void addWindowListener(){
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e)
			{
				if(!saved)  displayConfirmExitDialog();
				else System.exit(0);
			}
		});
	}
	private void saveModifications(){
		//bank.saveHash();
		//bank.savePersons();
		saved=true;
	}
	/**
	 * Method to display the exit confirmation dialog
	 */
	private void displayConfirmExitDialog(){
		int reply =JOptionPane.showConfirmDialog(null, "Do you want to save the modifications ?","Confirm exit",JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.NO_OPTION)
		{
			System.exit(0);
		}
		if (reply == JOptionPane.YES_OPTION)
		{
			saveModifications();
			System.exit(0);
		}
	}
	/** 
	 * Method that adds listeners to the buttons and combo boxes
	 */
	private void addListeners(){

		class ClickListener implements ActionListener{

			public void actionPerformed (ActionEvent e){
				String s=e.getActionCommand();
				if (s.equals("Delete")){
					if (okAccount){
						if (deleteKeyField.getText().isEmpty()) JOptionPane.showMessageDialog(deleteDialog,"Please enter a value!");
						else {

						//	bank.deleteAccount(Integer.parseInt(deleteKeyField.getText()));		
							saved=false;

						}
					}
					else {
						if (deleteKeyField.getText().isEmpty()) JOptionPane.showMessageDialog(deleteDialog,"Please enter a value!");
						else {

						//	bank.deletePerson(Integer.parseInt(deleteKeyField.getText()));	
							saved=false;

						}
					}
				}
				else if (s.equals("Find")){
					if (okAccount){
						if (entryNumber.getText().isEmpty()) JOptionPane.showMessageDialog(findDialog,"Please enter a value!");
						else {

						//	bank.findAccount(Integer.parseInt(entryNumber.getText()));

						}
					}
					else{
						if (entryNumber.getText().isEmpty()) JOptionPane.showMessageDialog(findDialog,"Please enter a value!");
						else {

						//	bank.findPerson(entryNumber.getText());

						}
					}
				}
				else if (s.equals("Person")){
					okAccount=false;
				}
				else if (s.equals("Account")){
					okAccount=true;
				}
				else if (s.equals("New person")){

					newPersonDialog = new JDialog();
					newPersonDialog.setSize(300,200);
					newPersonDialog.setLayout(new GridLayout(5,2));
					newPersonDialog.add(personIdLabel);
					newPersonDialog.add(personIdField);
					newPersonDialog.add(personNameLabel);
					newPersonDialog.add(personNameField);
					newPersonDialog.add(personAddressLabel);
					newPersonDialog.add(personAddressField);
					newPersonDialog.add(newPersonOkButton);
					newPersonDialog.setVisible(true);

				}
				else if (s.equals("Add person")){
					if (personNameField.getText().isEmpty()||personAddressField.getText().isEmpty()||personIdField.getText().isEmpty()) JOptionPane.showMessageDialog(newPersonDialog,"Please enter all values!");
					else {

						//bank.insertPerson(new Person (Integer.parseInt(personIdField.getText()),personNameField.getText(),personAddressField.getText()));												
						saved=false;
					}
				}
				else if (s.equals("New account")){
					//for (int i=personCombo.getItemCount();i<bank.returnNames().length;i++)
				//		personCombo.addItem(bank.returnNames()[i]);

					newAccountDialog = new JDialog();
					newAccountDialog.setSize(400,300);
					newAccountDialog.setLayout(new GridLayout(9,2));
					newAccountDialog.add(accountIdLabel);
					newAccountDialog.add(accountIdField);
					newAccountDialog.add(accountOwnerLabel);
					newAccountDialog.add(personCombo);
					newAccountDialog.add(accountAmountLabel);
					newAccountDialog.add(accountAmountField);
					newAccountDialog.add(interestLabel);
					newAccountDialog.add(interestField);
					newAccountDialog.add(minimumMonthsLabel);
					newAccountDialog.add(minimumMonthsField);
					ButtonGroup group = new ButtonGroup();
					group.add(savingAccount);
					group.add(spendingAccount);
					newAccountDialog.add(savingAccount);
					newAccountDialog.add(spendingAccount);
					newAccountDialog.add(newAccountOkButton);
					newAccountDialog.setVisible(true);
				}
				else if (s.equals("Add account")){

					if (accountAmountField.getText().isEmpty()||accountIdField.getText().isEmpty()||personCombo.getSelectedItem()==null) JOptionPane.showMessageDialog(newAccountDialog,"Please enter all values!");
					else {

						if (isSavingAccount){
							SavingAccount a;
	//						a = new SavingAccount (Integer.parseInt(accountIdField.getText()),bank.findPerson((String) personCombo.getSelectedItem()),Integer.parseInt(accountAmountField.getText()),Integer.parseInt(interestField.getText()),Integer.parseInt(minimumMonthsField.getText()));
	//						bank.insertSavingAccount(Integer.parseInt(accountIdField.getText()), a);
						}
						else{
							SpendingAccount a;
			//				a = new SpendingAccount (Integer.parseInt(accountIdField.getText()),bank.findPerson((String) personCombo.getSelectedItem()),Integer.parseInt(accountAmountField.getText()));
			//				bank.insertSpendingAccount(Integer.parseInt(accountIdField.getText()), a);
						}

						saved=false;						

					}
				}
				else if (s.equals("Show balance")){
			//		for (int i=ownerCombo.getItemCount();i<bank.returnNames().length;i++)
		//				ownerCombo.addItem(bank.returnNames()[i]);
					accountBalanceDialog = new JDialog();
					accountBalanceDialog.setSize(300,200);
					accountBalanceDialog.setLayout(new GridLayout(5,2));		
					accountBalanceDialog.add(ownerNameLabel);
					accountBalanceDialog.add(ownerCombo);
					accountBalanceDialog.add(accountBalanceOkButton);
					accountBalanceDialog.setVisible(true);
				}
				else if (s.equals("Balance")){
					if (ownerCombo.getSelectedItem()==null) JOptionPane.showMessageDialog(accountBalanceDialog,"Please select your name!");
					else {

				//		bank.findBalance((String)ownerCombo.getSelectedItem());


					}
				}
				else if (s.equals("Saving account")){
					isSavingAccount=true;
				}
				else if (s.equals("Spending account")){
					isSavingAccount=false;
				}
				else if (s.equals("Extract")){
					if (extractAmountField.getText().isEmpty()||ownerExtractCombo.getSelectedItem()==null) JOptionPane.showMessageDialog(extractDialog,"Please enter all values!");
					else {
					//	bank.extract((String)ownerExtractCombo.getSelectedItem(), Integer.parseInt(extractAmountField.getText()), isSavingAccount);
						saved=false;						

					}
				}
				else if (s.equals("Deposit")){
					if (depositAmountField.getText().isEmpty()||ownerDepositCombo.getSelectedItem()==null) JOptionPane.showMessageDialog(depositDialog,"Please enter all values!");
					else {
				//		bank.deposit((String)ownerDepositCombo.getSelectedItem(), Integer.parseInt(depositAmountField.getText()), isSavingAccount);
						saved=false;						

					}
				}
			}
		}
		class ComboListener implements ItemListener{

			public void itemStateChanged(ItemEvent evt) {


				// Get the affected item
				Object item = evt.getItem();

				if (evt.getStateChange() == ItemEvent.SELECTED) {
					// Item was just selected

					if (item.toString().equals("Show")){
					//	if (okAccount)
					//		bank.showAccounts();
				//		else bank.showPersons();
					}

					else if (item.toString().equals("Delete")){
						deleteDialog = new JDialog();
						deleteDialog.setSize(200,200);

						deleteDialog.setLayout(new GridLayout(5,2));
						deleteDialog.add(deleteKeyLabel);
						deleteDialog.add(deleteKeyField);
						deleteDialog.add(deleteOk);
						deleteDialog.setVisible(true);
					}

					else if (item.toString().equals("Find")){
						findDialog = new JDialog();
						findDialog.setSize(200,200);

						findDialog.setLayout(new GridLayout(5,2));
						findDialog.add(entryNumberLabel);
						findDialog.add(entryNumber);
						findDialog.add(findOk);
						findDialog.setVisible(true);
					}
					else if (item.toString().equals("Deposit")){
				//		for (int i=ownerDepositCombo.getItemCount();i<bank.returnNames().length;i++)
				//			ownerDepositCombo.addItem(bank.returnNames()[i]);
						depositDialog = new JDialog();
						depositDialog.setSize(200,200);

						depositDialog.setLayout(new GridLayout(5,2));
						depositDialog.add(ownerNameLabel);
						depositDialog.add(ownerDepositCombo);
						depositDialog.add(accountAmountLabel);
						depositDialog.add(depositAmountField);
						ButtonGroup group = new ButtonGroup();
						group.add(savingAccount);
						group.add(spendingAccount);
						depositDialog.add(savingAccount);
						depositDialog.add(spendingAccount);
						depositDialog.add(depositButton);
						depositDialog.setVisible(true);
					}
					else if (item.toString().equals("Extract")){
					//	for (int i=ownerExtractCombo.getItemCount();i<bank.returnNames().length;i++)
					//		ownerExtractCombo.addItem(bank.returnNames()[i]);
						extractDialog = new JDialog();
						extractDialog.setSize(200,200);

						extractDialog.setLayout(new GridLayout(5,2));
						extractDialog.add(ownerNameLabel);
						extractDialog.add(ownerExtractCombo);
						extractDialog.add(accountAmountLabel);
						extractDialog.add(extractAmountField);
						ButtonGroup group = new ButtonGroup();
						group.add(savingAccount);
						group.add(spendingAccount);
						extractDialog.add(savingAccount);
						extractDialog.add(spendingAccount);
						extractDialog.add(extractButton);
						extractDialog.setVisible(true);
					}
					else if (item.toString().equals("Save")){
						saveModifications();
					}
				} else if (evt.getStateChange() == ItemEvent.DESELECTED) {
					// Item is no longer selected
				}
			}


		}
		findOk.addActionListener(new ClickListener());
		opCombo.addItemListener(new ComboListener());
		insertOk.addActionListener(new ClickListener());
		deleteOk.addActionListener(new ClickListener());
		personHash.addActionListener(new ClickListener());
		accountHash.addActionListener(new ClickListener());
		newPersonButton.addActionListener (new ClickListener());
		newPersonOkButton.addActionListener (new ClickListener());
		newAccountButton.addActionListener (new ClickListener());
		newAccountOkButton.addActionListener (new ClickListener());
		accountBalanceButton.addActionListener (new ClickListener());
		accountBalanceOkButton.addActionListener (new ClickListener());
		savingAccount.addActionListener (new ClickListener());
		spendingAccount.addActionListener (new ClickListener());
		extractButton.addActionListener (new ClickListener());
		depositButton.addActionListener (new ClickListener());

	}
/*private JMenuBar meniu;
	
	

	
	private JMenuItem eMenuItem;
	private JMenuItem showAccounts;
	private JMenuItem insertAccount;
	private JMenuItem deleteAccount;
	private JMenuItem DepositMoney;
	private JMenuItem extractMoney;
	private JMenuItem searchId;
	private JMenuItem searchCNP;


	
	
	
	private JTextField formNumeText;
	private JTextField formIdText;
	private JTextField formPrenumeText;
	private JTextField formCNPText;
	private JTextField formMoneyAmountText;
private  JTable searchAccounts(String id, String mesaj){
private JLabel formNume;
	private JLabel formId;
	private JLabel formPrenume;
	private JLabel formCNP;
		/**
		 * Add a new account for a person
		
		else if (event.getSource() == btnAddAccount) {
			
			String name = checkInput.checkString(textName);
			String email = checkInput.checkString(textEmail);
			String phone = checkInput.checkString(textPhone);
			int pin = checkInput.checkNumber(textId);
			int sum = checkInput.checkNumber(textMoney);

			if (name != null && email != null && phone != null && pin != 0 && sum != 0) {
				String choice = JOptionPane.showInputDialog(this,
						"Please enter: 1 for spending account or 2 for saving account ");
				Person p = new Person(name, email, phone);
				if (choice.equals("1")) {
					Account acc = new SpendingAccount(pin, sum);
					bank.addAccForPerson(p, acc);
					JOptionPane.showMessageDialog(this, "New spending account added for: " + p.toString());
				} else if (choice.equals("2")) {
					Account acc = new SavingAccount(pin, sum);
					bank.addAccForPerson(p, acc);
					JOptionPane.showMessageDialog(this, "New saving account added for: " + p.toString());
				} else {
					JOptionPane.showMessageDialog(this, "Please enter valid data!");
				}
				refresh();
				Object[] row1 = { name, email, phone, pin, sum };
				model.addRow(row1);
			} else
				JOptionPane.showMessageDialog(this, "Please enter valid data!");

		}

		else if (event.getSource() == btnDeletePerson) {
			String name = checkInput.checkString(textName);
			String email = checkInput.checkString(textEmail);
			String phone = checkInput.checkString(textPhone);

			if (name != null && email != null && phone != null) {
				Person p = new Person(name, email, phone);
				bank.deletePerson(p);
			} else {
				JOptionPane.showMessageDialog(this, "Please enter valid data!");
			}
			refresh();
			int i = table.getSelectedRow();
			if (i >= 0) {
				model.removeRow(i);
				refresh();
			} else {
				System.out.println("Delete error");
			}
		}


		else if (event.getSource() == btnDeleteAccount) {

			String name = checkInput.checkString(textName);
			String email = checkInput.checkString(textEmail);
			String phone = checkInput.checkString(textPhone);
			int pin = checkInput.checkNumber(textId);

			if (name != null && email != null && phone != null && pin != 0) {
				Person p = new Person(name, email, phone);
				bank.deleteAccount(pin, p);
			}
			refresh();
		}

		else if (event.getSource() == findAllAccounts) {
			Bank newBank = new Bank();
			try {
				newBank = ser.DeserializeBank();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Set<Account> allAccounts = newBank.findAllAccounts();
			for (Account acc : allAccounts) {
				System.out.println(acc.toString());
				String id = Integer.toString(acc.getAccId());
				String money = Double.toString(acc.getMoney());
				Object[] row = { "", "", "", id, money };
				model.addRow(row);
			}
			Object[] row1 = { "", "", "", "", "" };
			model.addRow(row1);
		}

		else if (event.getSource() == findAllPersons) {
			Bank newBank = new Bank();
			try {
				newBank = ser.DeserializeBank();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			Iterator<Entry<Person, Set<Account>>> it = newBank.getList().entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Person, Set<Account>> pair = it.next();
				Person p = pair.getKey();
				String name = p.getName();
				String email = p.getEmail();
				String phone = p.getPhone();

				Set<Account> accForOnePerson = pair.getValue();
				for (Account acc : accForOnePerson) {
					String pin = Integer.toString(acc.getAccId());
					String money = Double.toString(acc.getMoney());
					Object[] row = { name, email, phone, pin, money };
					model.addRow(row);
				}
			}
			Object[] emptyRow = { "", "", "", "", "" };
			model.addRow(emptyRow);

		}

		
		else if (event.getSource() == saveBank) {
			ser.SerializeBank(bank);
		}
	}

	public  void changeRows() {

		Object[] row = new Object[5];
		row[0] = textName.getText();
		row[1] = textEmail.getText();
		row[2] = textPhone.getText();
		row[3] = textId.getText();
		row[4] = textMoney.getText();
		model.addRow(row);
	}
	private JLabel formMoneyAmount;
	
	private JScrollPane scrollPane;
	private JTable tableAccounts;
	private JTable tableAcc;
	private  JTable tableAc;
	protected Bank bank;
	private int iddAccounts;
	
	private JRadioButton savingRadio;
	private JRadioButton spendingRadio;
	JTable table = searchAccount(id, mesaj);
	if (table == null)
	private JButton insertButton;
	private JButton deleteButton;
	private JButton enter;
	private JButton extractButton;
	private JButton depositButton;
	private JButton searchButton;
		return null;
	table.setPreferredScrollableViewportSize(table.getPreferredSize()); 
		private JMenu file;
	private JMenu Accounts; 
	private JMenu Operations;
	private JMenu search;
	return table;
}
	
*/public static void main(String[] args){

}
	
}

